<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;

/**
 * Tipocomorbilidad
 *
 * @ORM\Table(name="tipocomorbilidad", indexes={@ORM\Index(name="fk_tipocomorbilidad_perfilbioquimco1", columns={"perfilbioquimco_idperfilbioquimco"})})
 * @ORM\Entity
 */
class Tipocomorbilidad implements \JsonSerializable
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string|null
     *
     * @ORM\Column(name="descripcion", type="string", length=45, nullable=true, options={"default"="NULL"})
     */
    private $descripcion = 'NULL';

    /**
     * @var \Perfilbioquimco
     *
     * @ORM\ManyToOne(targetEntity="Perfilbioquimco", inversedBy="comorbilidad")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="perfilbioquimco_idperfilbioquimco", referencedColumnName="id")
     * })
     */
    private $perfilBioquimico;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getDescripcion(): ?string
    {
        return $this->descripcion;
    }

    public function setDescripcion(?string $descripcion): self
    {
        $this->descripcion = $descripcion;

        return $this;
    }

    public function getPerfilbioquimcoIdperfilbioquimco(): ?Perfilbioquimco
    {
        return $this->perfilBioquimico;
    }

    public function setPerfilbioquimcoIdperfilbioquimco(?Perfilbioquimco $perfilBioquimico): self
    {
        $this->perfilBioquimico = $perfilBioquimico;

        return $this;
    }

    public function jsonSerialize(): array{

        return [
            'id' => $this->id,
            'descripcion' => $this->descripcion,
            'perfilBioquimico' => $this->perfilBioquimico
        ];
        
    }

}
