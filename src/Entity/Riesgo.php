<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Riesgo
 *
 * @ORM\Table(name="riesgo", indexes={@ORM\Index(name="fk_riesgo_tipocomorbilidad1", columns={"tipocomorbilidad_id_tipocomorbilidad"})})
 * @ORM\Entity
 */
class Riesgo
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string|null
     *
     * @ORM\Column(name="descripcion", type="string", length=45, nullable=true, options={"default"="NULL"})
     */
    private $descripcion = 'NULL';

    /**
     * @var string|null
     *
     * @ORM\Column(name="trastorno", type="string", length=45, nullable=true, options={"default"="NULL"})
     */
    private $trastorno = 'NULL';

    /**
     * @var int|null
     *
     * @ORM\Column(name="comorbilidades_id_comorbilidad", type="integer", nullable=true, options={"default"="NULL"})
     */
    private $comorbilidadesIdComorbilidad = 'NULL';

    /**
     * @var \Tipocomorbilidad
     *
     * @ORM\ManyToOne(targetEntity="Tipocomorbilidad")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="tipocomorbilidad_id_tipocomorbilidad", referencedColumnName="id")
     * })
     */
    private $tipocomorbilidadIdTipocomorbilidad;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getDescripcion(): ?string
    {
        return $this->descripcion;
    }

    public function setDescripcion(?string $descripcion): self
    {
        $this->descripcion = $descripcion;

        return $this;
    }

    public function getTrastorno(): ?string
    {
        return $this->trastorno;
    }

    public function setTrastorno(?string $trastorno): self
    {
        $this->trastorno = $trastorno;

        return $this;
    }

    public function getComorbilidadesIdComorbilidad(): ?int
    {
        return $this->comorbilidadesIdComorbilidad;
    }

    public function setComorbilidadesIdComorbilidad(?int $comorbilidadesIdComorbilidad): self
    {
        $this->comorbilidadesIdComorbilidad = $comorbilidadesIdComorbilidad;

        return $this;
    }

    public function getTipocomorbilidadIdTipocomorbilidad(): ?Tipocomorbilidad
    {
        return $this->tipocomorbilidadIdTipocomorbilidad;
    }

    public function setTipocomorbilidadIdTipocomorbilidad(?Tipocomorbilidad $tipocomorbilidadIdTipocomorbilidad): self
    {
        $this->tipocomorbilidadIdTipocomorbilidad = $tipocomorbilidadIdTipocomorbilidad;

        return $this;
    }


}
