<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;

/**
 * Control
 *
 * @ORM\Table(name="control", indexes={@ORM\Index(name="fk_Control_indices1", columns={"id_indice"}), @ORM\Index(name="fk_Control_perfilbioquimco1", columns={"perfilbioquimco_idperfilbioquimco"}), @ORM\Index(name="fk_Control_tipocomorbilidad1", columns={"id_tipocomorbilidad"}), @ORM\Index(name="fk_usuario_id", columns={"usuario_id"}), @ORM\Index(name="fk_Control_antropometría1", columns={"id_antropometria"}), @ORM\Index(name="fk_Control_tipo_morfotipo1", columns={"id_tipo_morfotipo"}), @ORM\Index(name="fk_Control_paciente1", columns={"paciente_id_paciente"})})
 * @ORM\Entity
 */
class Control implements \JsonSerializable
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string|null
     *
     * @ORM\Column(name="fecha", type="string", nullable=true, options={"default"="NULL"})
     */
    private $fecha = 'NULL';

    /**
     * @var string|null
     *
     * @ORM\Column(name="observaciones", type="string", length=45, nullable=true, options={"default"="NULL"})
     */
    private $observaciones = 'NULL';

    /**
     * @var \Antropometria
     *
     * @ORM\ManyToOne(targetEntity="Antropometria", inversedBy="control")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_antropometria", referencedColumnName="id")
     * })
     */
    private $idAntropometria;

    /**
     * @var \Indices
     *
     * @ORM\ManyToOne(targetEntity="Indices", inversedBy="control")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_indice", referencedColumnName="id")
     * })
     */
    private $idIndice;

    /**
     * @var \Paciente
     *
     * @ORM\ManyToOne(targetEntity="Paciente", inversedBy="control")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="paciente_id_paciente", referencedColumnName="id")
     * })
     */
    private $pacienteIdPaciente;

    /**
     * @var \Perfilbioquimco
     *
     * @ORM\ManyToOne(targetEntity="Perfilbioquimco", inversedBy="control")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="perfilbioquimco_idperfilbioquimco", referencedColumnName="id")
     * })
     */
    private $perfilbioquimcoIdperfilbioquimco;

    /**
     * @var \Morfotipo
     *
     * @ORM\ManyToOne(targetEntity="Morfotipo", inversedBy="control")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_tipo_morfotipo", referencedColumnName="id")
     * })
     */
    private $idTipoMorfotipo;

    /**
     * @var \Tipocomorbilidad
     *
     * @ORM\ManyToOne(targetEntity="Tipocomorbilidad", inversedBy="control")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_tipocomorbilidad", referencedColumnName="id")
     * })
     */
    private $idTipocomorbilidad;

    /**
     * @var \Usuarios
     *
     * @ORM\ManyToOne(targetEntity="Usuarios", inversedBy="control")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="usuario_id", referencedColumnName="id")
     * })
     */
    private $usuario;

    /**
     * @var \TipoIntervencion
     *
     * @ORM\ManyToOne(targetEntity="TipoIntervencion", inversedBy="control")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="tipo_intervencion_id", referencedColumnName="id")
     * })
     */
    private $tipoIntervencion;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getFecha(): ?string
    {
        return $this->fecha;
    }

    public function setFecha(?string $fecha): self
    {
        $this->fecha = $fecha;

        return $this;
    }

    public function getObservaciones(): ?string
    {
        return $this->observaciones;
    }

    public function setObservaciones(?string $observaciones): self
    {
        $this->observaciones = $observaciones;

        return $this;
    }

    public function getIdIndice(): ?Indices
    {
        return $this->idIndice;
    }

    public function setIdIndice(?Indices $idIndice): self
    {
        $this->idIndice = $idIndice;

        return $this;
    }

    public function getAntropometria(): ?Antropometria
    {
        return $this->idAntropometria;
    }

    public function setAntropometria(?Antropometria $idAntropometria): self
    {
        $this->idAntropometria = $idAntropometria;

        return $this;
    }

    public function getPacienteIdPaciente(): ?Paciente
    {
        return $this->pacienteIdPaciente;
    }

    public function setPacienteIdPaciente(?Paciente $pacienteIdPaciente): self
    {
        $this->pacienteIdPaciente = $pacienteIdPaciente;

        return $this;
    }

    public function getPerfilbioquimcoIdperfilbioquimco(): ?Perfilbioquimco
    {
        return $this->perfilbioquimcoIdperfilbioquimco;
    }

    public function setPerfilbioquimcoIdperfilbioquimco(?Perfilbioquimco $perfilbioquimcoIdperfilbioquimco): self
    {
        $this->perfilbioquimcoIdperfilbioquimco = $perfilbioquimcoIdperfilbioquimco;

        return $this;
    }

    public function getIdTipoMorfotipo(): ?Morfotipo
    {
        return $this->idTipoMorfotipo;
    }

    public function setIdTipoMorfotipo(?Morfotipo $idTipoMorfotipo): self
    {
        $this->idTipoMorfotipo = $idTipoMorfotipo;

        return $this;
    }

    public function getIdTipocomorbilidad(): ?Tipocomorbilidad
    {
        return $this->idTipocomorbilidad;
    }

    public function setIdTipocomorbilidad(?Tipocomorbilidad $idTipocomorbilidad): self
    {
        $this->idTipocomorbilidad = $idTipocomorbilidad;

        return $this;
    }

    public function getUsuario(): ?Usuarios
    {
        return $this->usuario;
    }

    public function setUsuario(?Usuarios $usuario): self
    {
        $this->usuario = $usuario;

        return $this;
    }

    public function getTipoIntervencion(): ?TipoIntervencion
    {
        return $this->tipoIntervencion;
    }

    public function setTipoIntervencion(?TipoIntervencion $tipoIntervencion): self
    {
        $this->tipoIntervencion = $tipoIntervencion;

        return $this;
    }

    public function jsonSerialize(): array{

        return [
            'id' => $this->id,
            'fecha' => $this->fecha,
            'observaciones' => $this->observaciones,
            'idAntropometria' => $this->idAntropometria,
            'idIndice' => $this->idIndice,
            'pacienteIdPaciente' => $this->pacienteIdPaciente,
            'perfilbioquimcoIdperfilbioquimco' => $this->perfilbioquimcoIdperfilbioquimco,
            'idTipoMorfotipo' => $this->idTipoMorfotipo,
            'idTipocomorbilidad' => $this->idTipocomorbilidad,
            'usuario' => $this->usuario,
            'tipoIntervencion' => $this->tipoIntervencion
        ];

    }

}
