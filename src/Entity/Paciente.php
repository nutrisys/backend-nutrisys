<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;

/**
 * Paciente
 *
 * @ORM\Table(name="paciente", indexes={@ORM\Index(name="fk_paciente_usuario_id", columns={"usuario_id"})})
 * @ORM\Entity
 */
class Paciente implements \JsonSerializable
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string|null
     *
     * @ORM\Column(name="identificacion", type="string", nullable=true, options={"default"="NULL"})
     */
    private $identificacion = 'NULL';

    /**
     * @var string|null
     *
     * @ORM\Column(name="nombres", type="string", length=45, nullable=true, options={"default"="NULL"})
     */
    private $nombres = 'NULL';

    /**
     * @var string|null
     *
     * @ORM\Column(name="apellidos", type="string", length=45, nullable=true, options={"default"="NULL"})
     */
    private $apellidos = 'NULL';

    /**
     * @var string|null
     *
     * @ORM\Column(name="genero", type="string", length=2, nullable=true, options={"default"="NULL","fixed"=true})
     */
    private $genero = 'NULL';

    /**
     * @var string|null
     *
     * @ORM\Column(name="fechanacimiento", type="string", length=50, nullable=true, options={"default"="NULL"})
     */
    private $fechanacimiento = 'NULL';

    /**
     * @var string|null
     *
     * @ORM\Column(name="email", type="string", length=45, nullable=true, options={"default"="NULL"})
     */
    private $email = 'NULL';

    /**
     * @var string|null
     *
     * @ORM\Column(name="Direccion", type="string", length=45, nullable=true, options={"default"="NULL"})
     */
    private $direccion = 'NULL';

    /**
     * @var string|null
     *
     * @ORM\Column(name="telefono", type="string", nullable=true, options={"default"="NULL"})
     */
    private $telefono = 'NULL';

    // /**
    //  * @var string|null
    //  *
    //  * @ORM\Column(name="edad", type="string", nullable=true, options={"default"="NULL"})
    //  */
    // private $edad = 'NULL';

    /**
     * @var \Usuarios
     *
     * @ORM\ManyToOne(targetEntity="Usuarios", inversedBy="paciente")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="usuario_id", referencedColumnName="id")
     * })
     */
    private $usuario;

    //Relación con otras entidades
    //============================

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Control", mappedBy="pacienteIdPaciente")
    */

    private $control;

    public function __construct(){
       $this->control = new ArrayCollection();
   }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getIdentificacion(): ?string
    {
        return $this->identificacion;
    }

    public function setIdentificacion(?string $identificacion): self
    {
        $this->identificacion = $identificacion;

        return $this;
    }

    public function getNombres(): ?string
    {
        return $this->nombres;
    }

    public function setNombres(?string $nombres): self
    {
        $this->nombres = $nombres;

        return $this;
    }

    public function getApellidos(): ?string
    {
        return $this->apellidos;
    }

    public function setApellidos(?string $apellidos): self
    {
        $this->apellidos = $apellidos;

        return $this;
    }

    public function getGenero(): ?string
    {
        return $this->genero;
    }

    public function setGenero(?string $genero): self
    {
        $this->genero = $genero;

        return $this;
    }

    public function getFechaNacimiento(): ?string
    {
        return $this->fechanacimiento;
    }

    public function setFechaNacimiento(?string $fechanacimiento): self
    {
        $this->fechanacimiento = $fechanacimiento;

        return $this;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(?string $email): self
    {
        $this->email = $email;

        return $this;
    }

    public function getDireccion(): ?string
    {
        return $this->direccion;
    }

    public function setDireccion(?string $direccion): self
    {
        $this->direccion = $direccion;

        return $this;
    }

    public function getTelefono(): ?string
    {
        return $this->telefono;
    }

    public function setTelefono(?string $telefono): self
    {
        $this->telefono = $telefono;

        return $this;
    }

    // public function getEdad(): ?string
    // {
    //     return $this->edad;
    // }

    // public function setEdad(?string $edad): self
    // {
    //     $this->edad = $edad;

    //     return $this;
    // }

    public function getUsuario(): ?Usuarios
    {
        return $this->usuario;
    }

    public function setUsuario(?Usuarios $usuario): self
    {
        $this->usuario = $usuario;

        return $this;
    }

    public function jsonSerialize(): array{

        return [
            'id' => $this->id,
            'nombres' => $this->nombres,
            'apellidos' => $this->apellidos,
            'identificacion' => $this->identificacion,
            // 'edad' => $this->edad,
            'genero' => $this->genero,
            'fechaNaciemiento' => $this->fechanacimiento,
            'email' => $this->email,
            'direccion' => $this->direccion,
            'telefono' => $this->telefono,
            'medico' => $this->usuario
        ];
    }


}
