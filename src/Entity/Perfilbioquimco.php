<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;

/**
 * Perfilbioquimco
 *
 * @ORM\Table(name="perfilbioquimco")
 * @ORM\Entity
 */
class Perfilbioquimco implements \JsonSerializable
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string|null
     *
     * @ORM\Column(name="glucemia", type="string", nullable=true, options={"default"="NULL"})
     */
    private $glucemia = 'NULL';

    /**
     * @var string|null
     *
     * @ORM\Column(name="insulina", type="string", nullable=true, options={"default"="NULL"})
     */
    private $insulina = 'NULL';

    /**
     * @var string|null
     *
     * @ORM\Column(name="homair", type="string", nullable=true, options={"default"="NULL"})
     */
    private $homair = 'NULL';

    /**
     * @var string|null
     *
     * @ORM\Column(name="leptina", type="string", nullable=true, options={"default"="NULL"})
     */
    private $leptina = 'NULL';

    //Relación con otras entidades
    //============================

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Control", mappedBy="perfilbioquimcoIdperfilbioquimco")
     * @ORM\OneToMany(targetEntity="App\Entity\Tipocomorbilidad", mappedBy="perfilBioquimico")
    */

    private $control;
    private $comorbilidad;

    public function __construct(){
       $this->control = new ArrayCollection();
       $this->comorbilidad = new ArrayCollection();
   }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getGlucemia(): ?string
    {
        return $this->glucemia;
    }

    public function setGlucemia(?string $glucemia): self
    {
        $this->glucemia = $glucemia;

        return $this;
    }

    public function getInsulina(): ?string
    {
        return $this->insulina;
    }

    public function setInsulina(?string $insulina): self
    {
        $this->insulina = $insulina;

        return $this;
    }

    public function getHomair(): ?string
    {
        return $this->homair;
    }

    public function setHomair(?string $homair): self
    {
        $this->homair = $homair;

        return $this;
    }

    public function getLeptina(): ?string
    {
        return $this->leptina;
    }

    public function setLeptina(?string $leptina): self
    {
        $this->leptina = $leptina;

        return $this;
    }

    public function jsonSerialize(): array{

        return [
            'id' => $this->id,
            'glucemia' => $this->glucemia,
            'insulina' => $this->insulina,
            'homair' => $this->homair,
            'leptina' => $this->leptina
        ];
        
    }

}
