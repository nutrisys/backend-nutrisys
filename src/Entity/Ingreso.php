<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;

/**
 * Ingreso
 *
 * @ORM\Table(name="ingreso", indexes={@ORM\Index(name="fk_ingreso_medico1", columns={"usuario_id"})})
 * @ORM\Entity
 */
class Ingreso implements \JsonSerializable
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string|null
     *
     * @ORM\Column(name="usuario", type="string", length=45, nullable=true, options={"default"="NULL"})
     */
    private $usuario = 'NULL';

    /**
     * @var string|null
     *
     * @ORM\Column(name="contrasena", type="string", length=250, nullable=true, options={"default"="NULL"})
     */
    private $contrasena = 'NULL';

    /**
     * @var string|null
     *
     * @ORM\Column(name="fecha", type="string", length=45, nullable=true, options={"default"="NULL"})
     */
    private $fecha = 'NULL';

    /**
     * @var \Usuarios
     *
     * @ORM\ManyToOne(targetEntity="Usuarios", inversedBy="ingreso")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="usuario_id", referencedColumnName="id")
     * })
     */
    private $usuarioId;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getUsuario(): ?string
    {
        return $this->usuario;
    }

    public function setUsuario(?string $usuario): self
    {
        $this->usuario = $usuario;

        return $this;
    }

    public function getContrasena(): ?string
    {
        return $this->contrasena;
    }

    public function setContrasena(?string $contrasena): self
    {
        $this->contrasena = $contrasena;

        return $this;
    }

    public function getFecha(): ?string
    {
        return $this->fecha;
    }

    public function setFecha(?string $fecha): self
    {
        $this->fecha = $fecha;

        return $this;
    }

    public function getUsuarioId(): ?Usuarios
    {
        return $this->usuarioId;
    }

    public function setUsuarioId(?Usuarios $usuarioId): self
    {
        $this->usuarioId = $usuarioId;

        return $this;
    }

    public function jsonSerialize(): array{

        return [
            'id' => $this->id,
            'usuario' => $this->usuario,
            'password' => $this->contrasena,
            'fecha' => $this->fecha,
            'usuarioLogin' => $this->usuarioId
        ];
        
    }

}
