<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\Collection;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * Indices
 *
 * @ORM\Table(name="indices", indexes={@ORM\Index(name="fk_indices_morfotipo1", columns={"morfotipo_id_tipo_morfotipo"}), @ORM\Index(name="fk_indices_antropometría1", columns={"antropometría_id_antropometria"})})
 * @ORM\Entity
 */
class Indices implements \JsonSerializable
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string|null
     *
     * @ORM\Column(name="imc", type="string", nullable=true, options={"default"="NULL"})
     */
    private $imc = 'NULL';

    /**
     * @var string|null
     *
     * @ORM\Column(name="imgrasa", type="string", nullable=true, options={"default"="NULL"})
     */
    private $imgrasa = 'NULL';

    /**
     * @var string|null
     *
     * @ORM\Column(name="masagrasacorporal", type="string", nullable=true, options={"default"="NULL"})
     */
    private $masagrasacorporal = 'NULL';

    /**
     * @var string|null
     *
     * @ORM\Column(name="imlibredegrasa", type="string", nullable=true, options={"default"="NULL"})
     */
    private $imlibredegrasa = 'NULL';

    /**
     * @var string|null
     *
     * @ORM\Column(name="masalibredegrasa", type="string", nullable=true, options={"default"="NULL"})
     */
    private $masalibredegrasa = 'NULL';

    /**
     * @var string|null
     *
     * @ORM\Column(name="masamusculoesquelitica", type="string", nullable=true, options={"default"="NULL"})
     */
    private $masamusculoesquelitica = 'NULL';

    /**
     * @var string|null
     *
     * @ORM\Column(name="porcentaje_grasa_corporal", type="string", nullable=true, options={"default"="NULL"})
     */
    private $porcentajeGrasaCorporal = 'NULL';

    /**
     * @var string|null
     *
     * @ORM\Column(name="corpulencia", type="string", nullable=true, options={"default"="NULL"})
     */
    private $corpulencia = 'NULL';

    /**
     * @var string|null
     *
     * @ORM\Column(name="fenotipo_corporal", type="string", nullable=true, options={"default"="NULL"})
     */
    private $fenotipoCorporal = 'NULL';

    /**
     * @var \Antropometria
     *
     * @ORM\ManyToOne(targetEntity="Antropometria", inversedBy="indice")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="antropometría_id_antropometria", referencedColumnName="id")
     * })
     */
    private $antropometriaIdAntropometria;

    /**
     * @var \Morfotipo
     *
     * @ORM\ManyToOne(targetEntity="Morfotipo", inversedBy="indice")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="morfotipo_id_tipo_morfotipo", referencedColumnName="id")
     * })
     */
    private $morfotipoIdTipoMorfotipo;

    //Relación con otras entidades
    //============================

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Control", mappedBy="idIndice")
    */

    private $control;

    public function __construct(){
       $this->control = new ArrayCollection();
   }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getImc(): ?string
    {
        return $this->imc;
    }

    public function setImc(?string $imc): self
    {
        $this->imc = $imc;

        return $this;
    }

    public function getImgrasa(): ?string
    {
        return $this->imgrasa;
    }

    public function setImgrasa(?string $imgrasa): self
    {
        $this->imgrasa = $imgrasa;

        return $this;
    }

    public function getMasagrasacorporal(): ?string
    {
        return $this->masagrasacorporal;
    }

    public function setMasagrasacorporal(?string $masagrasacorporal): self
    {
        $this->masagrasacorporal = $masagrasacorporal;

        return $this;
    }

    public function getImlibredegrasa(): ?string
    {
        return $this->imlibredegrasa;
    }

    public function setImlibredegrasa(?string $imlibredegrasa): self
    {
        $this->imlibredegrasa = $imlibredegrasa;

        return $this;
    }

    public function getMasalibredegrasa(): ?string
    {
        return $this->masalibredegrasa;
    }

    public function setMasalibredegrasa(?string $masalibredegrasa): self
    {
        $this->masalibredegrasa = $masalibredegrasa;

        return $this;
    }

    public function getMasamusculoesquelitica(): ?string
    {
        return $this->masamusculoesquelitica;
    }

    public function setMasamusculoesquelitica(?string $masamusculoesquelitica): self
    {
        $this->masamusculoesquelitica = $masamusculoesquelitica;

        return $this;
    }

    public function getCorpulencia(): ?string
    {
        return $this->corpulencia;
    }

    public function setCorpulencia(?string $corpulencia): self
    {
        $this->corpulencia = $corpulencia;

        return $this;
    }

    public function getFenotipoCorporal(): ?string
    {
        return $this->fenotipoCorporal;
    }

    public function setFenotipoCorporal(?string $fenotipoCorporal): self
    {
        $this->fenotipoCorporal = $fenotipoCorporal;

        return $this;
    }

    public function getMorfotipoIdTipoMorfotipo(): ?Morfotipo
    {
        return $this->morfotipoIdTipoMorfotipo;
    }

    public function setMorfotipoIdTipoMorfotipo(?Morfotipo $morfotipoIdTipoMorfotipo): self
    {
        $this->morfotipoIdTipoMorfotipo = $morfotipoIdTipoMorfotipo;

        return $this;
    }

    public function getAntropometria(): ?Antropometria
    {
        return $this->antropometriaIdAntropometria;
    }

    public function setAntropometria(?Antropometria $antropometriaIdAntropometria): self
    {
        $this->antropometriaIdAntropometria = $antropometriaIdAntropometria;

        return $this;
    }

    public function getPorcentajeGrasa(): ?string
    {
        return $this->porcentajeGrasaCorporal;
    }

    public function setPorcentajeGrasa(?string $porcentajeGrasaCorporal): self
    {
        $this->porcentajeGrasaCorporal = $porcentajeGrasaCorporal;

        return $this;
    }

    public function jsonSerialize(): array{

        return [
            'id' => $this->id,
            'imc' => $this->imc,
            'imgrasa' => $this->imgrasa,
            'masagrasacorporal' => $this->masagrasacorporal,
            'imlibredegrasa' => $this->imlibredegrasa,
            'masalibredegrasa' => $this->masalibredegrasa,
            'masamusculoesquelitica' => $this->masamusculoesquelitica,
            'porcentajeGrasaCorporal' => $this->porcentajeGrasaCorporal,
            'antropometriaIdAntropometria' => $this->antropometriaIdAntropometria,
            'morfotipoIdTipoMorfotipo' => $this->morfotipoIdTipoMorfotipo,
            'corpulencia' => $this->corpulencia,
            'fenotipoCorporal' => $this->fenotipoCorporal
        ];
        
    }

}
