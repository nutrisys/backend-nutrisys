<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\Collection;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * TipoIntervencion
 *
 * @ORM\Table(name="tipo_intervencion", indexes={@ORM\Index(name="fk_intervencion_riesgo1", columns={"riesgo_id_riesgo"}), @ORM\Index(name="fk_tipo_intervencion_intervencion1", columns={"intervencion_idintervencion"}), @ORM\Index(name="fk_intervencion_tipo_morfotipo1", columns={"tipo_morfotipo_id_tipo_morfotipo"})})
 * @ORM\Entity
 */
class TipoIntervencion
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string|null
     *
     * @ORM\Column(name="observacion", type="string", length=45, nullable=true, options={"default"="NULL"})
     */
    private $observacion = 'NULL';

    /**
     * @var \Riesgo
     *
     * @ORM\ManyToOne(targetEntity="Riesgo")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="riesgo_id_riesgo", referencedColumnName="id")
     * })
     */
    private $riesgoIdRiesgo;

    /**
     * @var \Morfotipo
     *
     * @ORM\ManyToOne(targetEntity="Morfotipo")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="tipo_morfotipo_id_tipo_morfotipo", referencedColumnName="id")
     * })
     */
    private $tipoMorfotipoIdTipoMorfotipo;

    /**
     * @var \Intervencion
     *
     * @ORM\ManyToOne(targetEntity="Intervencion", inversedBy="intervencion")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="intervencion_idintervencion", referencedColumnName="id")
     * })
     */
    private $intervencionIdintervencion;

    //Relación con otras entidades
    //============================

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Control", mappedBy="tipoIntervencion")
    */

    private $control;

    public function __construct(){
       $this->control = new ArrayCollection();
   }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getObservacion(): ?string
    {
        return $this->observacion;
    }

    public function setObservacion(?string $observacion): self
    {
        $this->observacion = $observacion;

        return $this;
    }

    public function getRiesgoIdRiesgo(): ?Riesgo
    {
        return $this->riesgoIdRiesgo;
    }

    public function setRiesgoIdRiesgo(?Riesgo $riesgoIdRiesgo): self
    {
        $this->riesgoIdRiesgo = $riesgoIdRiesgo;

        return $this;
    }

    public function getTipoMorfotipoIdTipoMorfotipo(): ?Morfotipo
    {
        return $this->tipoMorfotipoIdTipoMorfotipo;
    }

    public function setTipoMorfotipoIdTipoMorfotipo(?Morfotipo $tipoMorfotipoIdTipoMorfotipo): self
    {
        $this->tipoMorfotipoIdTipoMorfotipo = $tipoMorfotipoIdTipoMorfotipo;

        return $this;
    }

    public function getIntervencion(): ?Intervencion
    {
        return $this->intervencionIdintervencion;
    }

    public function setIntervencion(?Intervencion $intervencionIdintervencion): self
    {
        $this->intervencionIdintervencion = $intervencionIdintervencion;

        return $this;
    }

    public function jsonSerialize(): array{

        return [
            'id' => $this->id,
            'intervencion' => $this->intervencionIdintervencion,
            'observacion' => $this->observacion
        ];

    }

}
