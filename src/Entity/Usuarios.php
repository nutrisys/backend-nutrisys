<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;

/**
 * Usuarios
 *
 * @ORM\Table(name="usuarios")
 * @ORM\Entity
 */
class Usuarios implements \JsonSerializable
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string|null
     *
     * @ORM\Column(name="nombre", type="string", length=45, nullable=true, options={"default"="NULL"})
     */
    private $nombre = 'NULL';

    /**
     * @var string|null
     *
     * @ORM\Column(name="apellido", type="string", length=45, nullable=true, options={"default"="NULL"})
     */
    private $apellido = 'NULL';

    /**
     * @var string|null
     *
     * @ORM\Column(name="identificacion", type="string", length=45, nullable=true, options={"default"="NULL"})
     */
    private $identificacion = 'NULL';

    /**
     * @var string|null
     *
     * @ORM\Column(name="rol", type="string", length=45, nullable=true, options={"default"="NULL"})
     */
    private $rol = 'NULL';

    /**
     * @var string|null
     *
     * @ORM\Column(name="registro_profesional", type="string", length=45, nullable=true, options={"default"="NULL"})
     */
    private $registroProfesional = 'NULL';

    //Relación con otras entidades
    //============================

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Ingreso", mappedBy="usuarioId")
     * @ORM\OneToMany(targetEntity="App\Entity\Paciente", mappedBy="usuario")
     * @ORM\OneToMany(targetEntity="App\Entity\Control", mappedBy="usuario")
    */

    private $ingreso;
    private $paciente;
    private $control;

    public function __construct(){
       $this->ingreso = new ArrayCollection();
       $this->paciente = new ArrayCollection();
       $this->control = new ArrayCollection();
   }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNombre(): ?string
    {
        return $this->nombre;
    }

    public function setNombre(?string $nombre): self
    {
        $this->nombre = $nombre;

        return $this;
    }

    public function getApellido(): ?string
    {
        return $this->apellido;
    }

    public function setApellido(?string $apellido): self
    {
        $this->apellido = $apellido;

        return $this;
    }

    public function getIdentificacion(): ?string
    {
        return $this->identificacion;
    }

    public function setIdentificacion(?string $identificacion): self
    {
        $this->identificacion = $identificacion;

        return $this;
    }

    public function getRol(): ?string
    {
        return $this->rol;
    }

    public function setRol(?string $rol): self
    {
        $this->rol = $rol;

        return $this;
    }

    public function getRegistroProfesional(): ?string
    {
        return $this->registroProfesional;
    }

    public function setRegistroProfesional(?string $registroProfesional): self
    {
        $this->registroProfesional = $registroProfesional;

        return $this;
    }

    public function jsonSerialize(): array{

        return [
            'id' => $this->id,
            'nombre' => $this->nombre,
            'apellido' => $this->apellido,
            'identificacion' => $this->identificacion,
            'rol' => $this->rol,
            'registroProfesional' => $this->registroProfesional
        ];
    }

}
