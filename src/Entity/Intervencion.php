<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\Collection;
use Doctrine\Common\Collections\ArrayCollection;
/**
 * Intervencion
 *
 * @ORM\Table(name="intervencion")
 * @ORM\Entity
 */
class Intervencion
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string|null
     *
     * @ORM\Column(name="descripcion", type="string", length=45, nullable=true, options={"default"="NULL"})
     */
    private $descripcion = 'NULL';

    //Relación con otras entidades
    //============================

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\TipoIntervencion", mappedBy="intervencionIdintervencion")
    */

    private $intervencion;

    public function __construct(){
       $this->intervencion = new ArrayCollection();
   }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getDescripcion(): ?string
    {
        return $this->descripcion;
    }

    public function setDescripcion(?string $descripcion): self
    {
        $this->descripcion = $descripcion;

        return $this;
    }

    public function jsonSerialize(): array{

        return [
            'id' => $this->id,
            'descripcion' => $this->descripcion
        ];

    }

}
