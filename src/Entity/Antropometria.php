<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;

/**
 * Antropometria
 *
 * @ORM\Table(name="antropometría")
 * @ORM\Entity
 */
class Antropometria implements \JsonSerializable
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string|null
     *
     * @ORM\Column(name="peso", type="string", length=45, nullable=true, options={"default"="NULL"})
     */
    private $peso = 'NULL';

    /**
     * @var string|null
     *
     * @ORM\Column(name="altura", type="string", length=45, nullable=true, options={"default"="NULL"})
     */
    private $altura = 'NULL';

    /**
     * @var string|null
     *
     * @ORM\Column(name="radiocaderacintura", type="string", length=45, nullable=true, options={"default"="NULL"})
     */
    private $radiocaderacintura = 'NULL';

    /**
     * @var string|null
     *
     * @ORM\Column(name="circunferenciamediacuello", type="string", length=45, nullable=true, options={"default"="NULL"})
     */
    private $circunferenciamediacuello = 'NULL';

    /**
     * @var string|null
     *
     * @ORM\Column(name="circunferenciamediaabdomen", type="string", length=45, nullable=true, options={"default"="NULL"})
     */
    private $circunferenciamediaabdomen = 'NULL';

    /**
     * @var string|null
     *
     * @ORM\Column(name="circunferenciamediacadera", type="string", length=45, nullable=true, options={"default"="NULL"})
     */
    private $circunferenciamediacadera = 'NULL';

    //Relación con otras entidades
    //============================

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Control", mappedBy="idAntropometria")
    */

    private $control;

    public function __construct(){
       $this->control = new ArrayCollection();
   }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getPeso(): ?string
    {
        return $this->peso;
    }

    public function setPeso(?string $peso): self
    {
        $this->peso = $peso;

        return $this;
    }

    public function getAltura(): ?string
    {
        return $this->altura;
    }

    public function setAltura(?string $altura): self
    {
        $this->altura = $altura;

        return $this;
    }

    public function getRadiocaderacintura(): ?string
    {
        return $this->radiocaderacintura;
    }

    public function setRadiocaderacintura(?string $radiocaderacintura): self
    {
        $this->radiocaderacintura = $radiocaderacintura;

        return $this;
    }

    public function getCircunferenciamediacuello(): ?string
    {
        return $this->circunferenciamediacuello;
    }

    public function setCircunferenciamediacuello(?string $circunferenciamediacuello): self
    {
        $this->circunferenciamediacuello = $circunferenciamediacuello;

        return $this;
    }

    public function getCircunferenciamediaabdomen(): ?string
    {
        return $this->circunferenciamediaabdomen;
    }

    public function setCircunferenciamediaabdomen(?string $circunferenciamediaabdomen): self
    {
        $this->circunferenciamediaabdomen = $circunferenciamediaabdomen;

        return $this;
    }

    public function getCircunferenciamediacadera(): ?string
    {
        return $this->circunferenciamediacadera;
    }

    public function setCircunferenciamediacadera(?string $circunferenciamediacadera): self
    {
        $this->circunferenciamediacadera = $circunferenciamediacadera;

        return $this;
    }

    public function jsonSerialize(): array{

        return [
            'id' => $this->id,
            'peso' => $this->peso,
            'altura' => $this->altura,
            'radioCaderaCintura' => $this->radiocaderacintura,
            'circunferenciaMediaCuello' => $this->circunferenciamediacuello,
            'circunferenciaMediaCadera' => $this->circunferenciamediacadera,
            'circunferenciaMediaAbdomen' => $this->circunferenciamediaabdomen
        ];
    }

}
