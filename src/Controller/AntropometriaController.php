<?php

namespace App\Controller;

use Symfony\Component\Validator\Validation;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Validator\Constraints\DateTime;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

use App\Entity\Indices;
use App\Entity\Control;
use App\Entity\Morfotipo;
use App\Services\JwtAuth;
use App\Entity\Antropometria;

class AntropometriaController extends AbstractController
{
 
    private function resjson($data){

        //Método para serializar los datos en formato json
        //================================================

        $json = $this->get('serializer')->serialize($data, 'json');
        $response = new Response();
        $response->setContent($json);
        $response->headers->set('Content-Type', 'application/json');

        return $response;

    }

    public function Registrar(Request $request, JwtAuth $jwt_auth){

        /*
            En este método se realiza el registro de todas las variables de antropometría para
            cada paciente.
            -----------------------------------------------------------------------------------
            NUTRISYS
        */

        $token = $request->headers->get('Authorization');
        $authCheck = $jwt_auth->checkToken($token);

        if($authCheck){
            
            $identity = $jwt_auth->checkToken($token, true);
            $doctrine = $this->getDoctrine();
            $em = $doctrine->getManager();

            //Obtener parametros del json
            //===========================

            $json = $request->get('json', null);
            $params = json_decode($json);

            $yearNow = new \DateTime('now', new \DateTimeZone('America/Bogota'));

            $morfotipo = $doctrine->getRepository(Morfotipo::class)->find(1);
            
            if($json != null){

                $peso = !empty($params->peso) ? $params->peso : null;
                $altura = !empty($params->altura) ? $params->altura : null;
                $idControl = !empty($params->idControl) ? $params->idControl : null;
                $radioCaderaCintura = !empty($params->radioCaderaCintura) ? $params->radioCaderaCintura : null;
                $circunferenciaMediaCuello = !empty($params->circunferenciaMediaCuello) ? $params->circunferenciaMediaCuello : null;
                $circunferenciaMediaCadera = !empty($params->circunferenciaMediaCadera) ? $params->circunferenciaMediaCadera : null;
                $circunferenciaMediaAbdomen = !empty($params->circunferenciaMediaAbdomen) ? $params->circunferenciaMediaAbdomen : null;

                if(!empty($peso) && !empty($altura) && !empty($radioCaderaCintura) && !empty($circunferenciaMediaCuello) &&
                   !empty($circunferenciaMediaCadera) && !empty($circunferenciaMediaAbdomen) && !empty($idControl)){

                    $control = $doctrine->getRepository(Control::class)->findOneBy([
                        'id' => $idControl
                    ]);

                    //Edad paciente
                    //=============

                    $yearBorn = new \DateTime($control->getPacienteIdPaciente()->getFechaNacimiento()); 
                    $edad = $yearNow->diff($yearBorn);

                    //Se realiza el cálculo de los indices
                    //====================================

                    $IMC = $peso / (pow(2, ($altura * 100)));

                    //Se valida el género del paciente
                    //================================

                    if($control->getPacienteIdPaciente()->getGenero() == 'M'){

                        $MGC = -52.335  + (2.159 * $circunferenciaMediaCadera) - (0.255 * $peso) - (167.571 * $radioCaderaCintura) - 
                               (1.455 * $IMC) + (0.904 * $circunferenciaMediaCuello) + (0.017 * $edad->y) - (0.082 * $altura) + 2.8931;


                    }else{

                        $MGC = -79.609 + (1.187 * $circunferenciaMediaCadera) - (0.491 * $IMC) + (0.316 * $peso) - (0.304 * $circunferenciaMediaAbdomen) + 
                               (64.064 * $radioCaderaCintura) + (0.017 * $edad->y) - (0.314 * $altura) + 2.23;

                    }

                    $IMG = $MGC / (pow(2, ($altura / 100)));

                    $MLG = -11.039 + (1.807 * $IMC) + (0.524 * $peso) - (1.735 * $IMG) + (0.327 * $altura) - (0.478 * $circunferenciaMediaCadera) - 
                           (26.169 * $radioCaderaCintura) - (0.296 * $MGC) - 0.644 + (0.158 * $circunferenciaMediaAbdomen) - (0.042 * $circunferenciaMediaAbdomen) + 0.3441818;

                    $IMLG = -96.615 - (1.255 * $IMG) + (77.445 * $radioCaderaCintura) + (0.968 * $circunferenciaMediaCadera) + (1.056 * $IMC) - (0.804 * $circunferenciaMediaAbdomen) + 
                            (0.445 * $circunferenciaMediaCuello) - (0.168 * $peso) + 0.61475;

                    $IMME = -6.403 - (0.711 * $IMG) + (0.173 * $circunferenciaMediaCuello) + (0.537 * $IMC) - (0.044 * $circunferenciaMediaAbdomen) + (0.110 * $MGC) - (0.078 * $peso) -
                            (0.285 * $IMLG) + (0.072 * $MLG) + (0.081 * $circunferenciaMediaCadera) + 0.0437;

                    $PCG= ($MGC / $peso) * 100;

                    //Nivel de corpulencia
                    //====================

                    if($IMC < 18.5){

                        $corpulencia = 'Peso Insuficiente';

                    }elseif($IMC < 25){

                        $corpulencia = 'Normopeso';

                    }elseif($IMC < 27){

                        $corpulencia = 'Sobrepeso Grado I';

                    }elseif($IMC < 30){

                        $corpulencia = 'Sobrepeso Grado II (Preobesisdad)';

                    }elseif($IMC < 35){

                        $corpulencia = 'Obesidad Tipo I';

                    }elseif($IMC < 40){

                        $corpulencia = 'Obesidad Tipo II';

                    }elseif($IMC < 50){

                        $corpulencia = 'Obesidad Tipo III (Mórbida)';

                    }else{

                        $corpulencia = 'Obesidad Tipo IV (Extrema)';

                    }

                    //Clasificación del fenotipo corporal
                    //===================================

                    if($control->getPacienteIdPaciente()->getGenero() == 'M'){

                        if($PCG < 13){

                            $fenotipo = 'Esencial';

                        }elseif($PCG < 20){

                            $fenotipo = 'Atleta';

                        }elseif($PCG < 24){

                            $fenotipo = 'Fitness';
                            
                        }elseif($PCG < 27){

                            $fenotipo = 'Aceptable';
                            
                        }elseif($PCG < 32){

                            $fenotipo = 'Exceso';

                        }else{

                            $fenotipo = 'Obesidad';

                        }

                    }else{

                        if($PCG < 15){

                            $fenotipo = 'Esencial';

                        }elseif($PCG < 20){

                            $fenotipo = 'Atleta';

                        }elseif($PCG < 25){

                            $fenotipo = 'Fitness';
                            
                        }elseif($PCG < 29){

                            $fenotipo = 'Aceptable';
                            
                        }elseif($PCG < 34){

                            $fenotipo = 'Exceso';

                        }else{

                            $fenotipo = 'Obesidad';

                        }

                    }

                    //Registro de las variables de antropometría
                    //==========================================

                    $antropometria = new Antropometria();
                    $antropometria->setPeso(round($peso,2));
                    $antropometria->setAltura(round($altura,2));
                    $antropometria->setRadiocaderacintura(round($radioCaderaCintura,2));
                    $antropometria->setCircunferenciamediacuello(round($circunferenciaMediaCuello,2));
                    $antropometria->setCircunferenciamediacadera(round($circunferenciaMediaCadera,2));
                    $antropometria->setCircunferenciamediaabdomen(round($circunferenciaMediaAbdomen,2));

                    $em->persist($antropometria);
                    $em->flush();

                    //Se registran los cálculos en Índices
                    //====================================

                    $indices = new Indices;
                    $indices->setImc($IMC);
                    $indices->setImgrasa(round($IMG,2));
                    $indices->setAntropometria($antropometria);
                    $indices->setImlibredegrasa(round($IMLG,2));
                    $indices->setPorcentajeGrasa(round($PCG,2));
                    $indices->setMasalibredegrasa(round($MLG,2));
                    $indices->setMasagrasacorporal(round($MGC,2));
                    $indices->setMorfotipoIdTipoMorfotipo($morfotipo);
                    $indices->setMasamusculoesquelitica(round($IMME,2));
                    $indices->setCorpulencia($corpulencia);
                    $indices->setFenotipoCorporal($fenotipo);
                    
                    $em->persist($indices);
                    $em->flush();

                    //Se actulizan las fkey de antropometria, indices y morfotipo en Control
                    //====================================================================== 

                    $control->setIdIndice($indices);
                    $control->setIdTipoMorfotipo($morfotipo);
                    $control->setAntropometria($antropometria);
                    
                    $em->persist($control);
                    $em->flush();

                    $dataClasificaciones = [
                        'nivelCorpulencia' => $corpulencia,
                        'fenotipoCorporal' => $fenotipo,
                        'antropometria' => $antropometria
                    ];

                    $data = [
                        'status' => 'success',
                        'code' => '200',
                        'message' => 'Información registrada con éxito',
                        'data' => $dataClasificaciones
                    ];

                }else{

                    $data = [
                        'status' => 'success',
                        'code' => '300',
                        'message' => 'Por favor complete todos los campos',
                    ];

                }

            }else{

                $data = [
                    'status' => 'error',
                    'code' => '400',
                    'message' => 'Json vacío',
                ];

            }

        }else{

            $data = [
                'status' => 'error',
                'code' => '100',
                'message' => 'Usuario no autenticado'
            ];

        }

        return $this->resjson($data);
       
    }

    public function Actualizar(Request $request, JwtAuth $jwt_auth){

        /*
            En este método se actualizan todas las variables de antropometría pertenecientes a 
            un paciente.
            ----------------------------------------------------------------------------------
            NUTRISYS
        */

        $token = $request->headers->get('Authorization');
        $authCheck = $jwt_auth->checkToken($token);

        if($authCheck){
            
            $identity = $jwt_auth->checkToken($token, true);
            $doctrine = $this->getDoctrine();
            $em = $doctrine->getManager();

            //Se obtiene parámetro
            //====================

            $idAntropometria = $request->get('idAntropometria');

            if(!empty($idAntropometria)){

                $antropometria = $doctrine->getRepository(Antropometria::class)->findOneBy([
                    'id' => $idAntropometria
                ]);

                if(!empty($antropometria)){

                    //Obtener parametros del json
                    //===========================

                    $json = $request->get('json', null);
                    $params = json_decode($json);
                    
                    if($json != null){

                        $peso = !empty($params->peso) ? $params->peso : $antropometria->getPeso();
                        $altura = !empty($params->altura) ? $params->altura : $antropometria->getAltura();
                        $radioCaderaCintura = !empty($params->radioCaderaCintura) ? $params->radioCaderaCintura : $antropometria->getRadiocaderacintura();
                        $circunferenciaMediaCuello = !empty($params->circunferenciaMediaCuello) ? $params->circunferenciaMediaCuello : $antropometria->getCircunferenciamediacuello();
                        $circunferenciaMediaCadera = !empty($params->circunferenciaMediaCadera) ? $params->circunferenciaMediaCadera : $antropometria->getCircunferenciamediacadera();
                        $circunferenciaMediaAbdomen = !empty($params->circunferenciaMediaAbdomen) ? $params->circunferenciaMediaAbdomen : $antropometria->getCircunferenciamediaabdomen();

                        //Actualización de las variables de antropometría
                        //===============================================

                        $antropometria->setPeso(round($peso,2));
                        $antropometria->setAltura(round($altura,2));
                        $antropometria->setRadiocaderacintura(round($radioCaderaCintura,2));
                        $antropometria->setCircunferenciamediacuello(round($circunferenciaMediaCuello,2));
                        $antropometria->setCircunferenciamediacadera(round($circunferenciaMediaCadera,2));
                        $antropometria->setCircunferenciamediaabdomen(round($circunferenciaMediaAbdomen,2));

                        $em->persist($antropometria);
                        $em->flush();

                        $data = [
                            'status' => 'success',
                            'code' => '200',
                            'message' => 'Información actualizada con éxito',
                            'data' => $antropometria
                        ];
                        
                    }else{

                        $data = [
                            'status' => 'error',
                            'code' => '400',
                            'message' => 'Json vacío',
                        ];

                    }

                }else{

                    $data = [
                        'status' => 'success',
                        'code' => '300',
                        'message' => 'No se encontró la antropometria con el id '.$idAntropometria,
                        'data' => 0
                    ];

                }

            }else{

                $data = [
                    'status' => 'success',
                    'code' => '300',
                    'message' => 'Por favor ingrese un id de antropometría'
                ];

            }

        }else{

            $data = [
                'status' => 'error',
                'code' => '100',
                'message' => 'Usuario no autenticado'
            ];

        }

        return $this->resjson($data);

    }

    public function Listar(Request $request, JwtAuth $jwt_auth){

        /*
          En este método se listan todos los registros almacenados en la tabla antropometría.
          -----------------------------------------------------------------------------------
          NUTRISYS
       */

       $token = $request->headers->get('Authorization');
       $authCheck = $jwt_auth->checkToken($token);

       if ($authCheck) {
          
           $identity = $jwt_auth->checkToken($token, true);
           $doctrine = $this->getDoctrine();
           $em = $doctrine->getManager();

           //Se obtiene la lista de antropometrías
           //=====================================

           $sqlAntropometrias = $em->createQueryBuilder()
              ->select('a') 
              ->from('App\Entity\Antropometria','a')
              ->orderBy('a.id', 'DESC')
           ;

          $result = $sqlAntropometrias->getQuery()->getResult();

           if(!empty($result)){

               $data = [
                   'status' => 'success',
                   'code' => '200',
                   'message' => 'Lista de antropometrías',
                   'data' => $result
               ];

           }else{

              $data = [
                  'status' => 'success',
                  'code' => '300',
                  'message' => 'No se encontraron antropometrías para listar',
                  'data' => 0
              ];

           } 

       }else{

               $data = [
                   'status' => 'error',
                   'code' => '100',
                   'message' => 'Usuario no autenticado'
               ];

       }

       return $this->resjson($data);

   }

}
