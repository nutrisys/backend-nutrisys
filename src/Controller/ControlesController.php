<?php

namespace App\Controller;

use Symfony\Component\Validator\Validation;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Validator\Constraints\DateTime;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

use App\Entity\Indices;
use App\Entity\Control;
use App\Entity\Paciente;
use App\Entity\Usuarios;
use App\Entity\Morfotipo;
use App\Services\JwtAuth;
use App\Entity\Antropometria;
use App\Entity\TipoComorbilidad;
use App\Entity\Perfilbioquimico;

class ControlesController extends AbstractController
{
    
    private function resjson($data){

        //Método para serializar los datos en formato json
        //================================================

        $json = $this->get('serializer')->serialize($data, 'json');
        $response = new Response();
        $response->setContent($json);
        $response->headers->set('Content-Type', 'application/json');

        return $response;

    }

    public function Registrar(Request $request, JwtAuth $jwt_auth){

        /*
            En este método se realiza el registro de controles para un paciente.
            --------------------------------------------------------------------
            NUTRISYS
        */

        $token = $request->headers->get('Authorization');
        $authCheck = $jwt_auth->checkToken($token);

        if($authCheck){
            
            $identity = $jwt_auth->checkToken($token, true);
            $doctrine = $this->getDoctrine();
            $em = $doctrine->getManager();

            //Obtener parámetro
            //=================

            $idPaciente = $request->get('idPaciente');
            $observacion = $request->get('observacion');
        
            $fecha = new \DateTime('now', new \DateTimeZone('America/Bogota'));
            $fechaControl = $fecha->format('Y-m-d H:i:s');

            $medico = $doctrine->getRepository(Usuarios::class)->findOneBy([
                'id' => $identity->sub
            ]);
            
            if(!empty($idPaciente)){

                $paciente = $doctrine->getRepository(Paciente::class)->findOneBy([
                    'id' => $idPaciente
                ]);

                if(!empty($descrpcion)){

                    $descrpcion = $descrpcion;

                }else{

                    $descrpcion = '';

                }

                if(!empty($paciente)){

                    //Registro del control
                    //====================

                    $control = new Control();
                    $control->setUsuario($medico);
                    $control->setFecha($fechaControl);
                    $control->setObservaciones($observacion);
                    $control->setPacienteIdPaciente($paciente);

                    $em->persist($control);
                    $em->flush();
    
                    $data = [
                        'status' => 'success',
                        'code' => '200',
                        'message' => 'Control registrado con éxito',
                        'data' => $control
                    ];

                }else{

                    $data = [
                        'status' => 'success',
                        'code' => '300',
                        'message' => 'No se encontró el paciente con el id '.$idPaciente,
                        'data' => 0
                    ];
                
                }

            }else{

                $data = [
                    'status' => 'success',
                    'code' => '300',
                    'message' => 'Por favor ingrese un id de paciente'
                ];

            }

        }else{

            $data = [
                'status' => 'error',
                'code' => '100',
                'message' => 'Usuario no autenticado'
            ];

        }

        return $this->resjson($data);

    }
}
