<?php

namespace App\Controller;

use Symfony\Component\Validator\Validation;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Validator\Constraints\DateTime;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

use App\Entity\Ingreso;
use App\Entity\Usuarios;
use App\Services\JwtAuth;

class UsuariosController extends AbstractController
{

    private function resjson($data){

        //Método para serializar los datos en formato json
        //================================================

        $json = $this->get('serializer')->serialize($data, 'json');
        $response = new Response();
        $response->setContent($json);
        $response->headers->set('Content-Type', 'application/json');

        return $response;

    }
    
    public function Registrar(Request $request, JwtAuth $jwt_auth){

        /*
            En este método se realiza el registro de clientes implementando autenticación.
            ------------------------------------------------------------------------------
            NUTRISYS
        */

        $token = $request->headers->get('Authorization');
        $authCheck = $jwt_auth->checkToken($token);

        if ($authCheck) {
            
            $identity = $jwt_auth->checkToken($token, true);
            $doctrine = $this->getDoctrine();
            $em = $doctrine->getManager();

            //Obtener parametros del json
            //===========================

            $json = $request->get('json', null);
            $params = json_decode($json); 

            $fecha = new \DateTime('now', new \DateTimeZone('America/Bogota'));
            $fechaRegistro = $fecha->format('Y-m-d H:i:s');

            if ($json = !null) {

                $rol = !empty($params->rol) ? $params->rol : null;
                $nombres = !empty($params->nombres) ? $params->nombres : null;
                $password = !empty($params->password) ? $params->password : null;
                $apellidos = !empty($params->apellidos) ? $params->apellidos : null;
                $documento = !empty($params->documento) ? $params->documento : null;
                $regsitroProfesional = !empty($params->regsitroProfesional) ? $params->regsitroProfesional : null;

                if(!empty($nombres) && !empty($apellidos) && !empty($rol) && !empty($documento) && !empty($password) && !empty($regsitroProfesional)) {

                    $nombresTrim = trim($nombres);
                    $nombres = $nombresTrim;

                    $apellidosTrim = trim($apellidos);
                    $apellidos = $apellidosTrim;

                    //Contraseña encriptada
                    //=====================

                    $pwd = hash('sha256', $password);

                    /*
                        Generamos el usuario de la persona con base en su primer nombre y la inicial de su
                        primer apellido. 
                    */

                    $numeroUser = 1;
                    $formatUser = explode(' ', $nombres);
                    $user = strtolower($formatUser[0] . $apellidos[0]);
                    $usuario = $user . $numeroUser;

                    $documentUnique = $doctrine->getRepository(Usuarios::class)->findOneBy([
                        'identificacion' => $documento
                    ]);

                    if (empty($documentUnique)) {

                        do {

                            $userUnique = $doctrine->getRepository(Ingreso::class)->findOneBy([
                                'usuario' => $usuario
                            ]); 

                            if (!empty($userUnique)) {

                                $numeroUser ++;
                                $usuario = $user . $numeroUser;

                            } else {

                                $usuario = $usuario;

                            }

                        } while (!empty($userUnique));

         
                        //Registro del usuario
                        //====================

                        $user = new Usuarios();
                        $user->setRol($rol);
                        $user->setIdentificacion($documento);
                        $user->setNombre(strtoupper($nombres));
                        $user->setApellido(strtoupper($apellidos));
                        $user->setRegistroProfesional($regsitroProfesional);

                        $em->persist($user);
                        $em->flush();
                        
                        //Se registra información de ingreso
                        //===================================

                        $ingreso = new Ingreso();
                        $ingreso->setContrasena($pwd);
                        $ingreso->setUsuarioId($user);
                        $ingreso->setUsuario($usuario);
                        $ingreso->setFecha($fechaRegistro);
                
                        $em->persist($ingreso);
                        $em->flush();
        
                        $data = [
                            'status' => 'success',
                            'code' => '200',
                            'message' => 'Usuario registrado con éxito',
                            'data' => $user
                        ];

                    }else{

                        $data = [
                            'status' => 'success',
                            'code' => '300',
                            'message' => 'Ya existe un usuario registrado con este número de documento'
                        ];
                    
                    }

                }else{

                    $data = [
                        'status' => 'success',
                        'code' => '300',
                        'message' => 'Por favor complete todos los campos',
                    ];

                }

            }else{

                $data = [
                    'status' => 'error',
                    'code' => '400',
                    'message' => 'Json vacío',
                ];

            }

        }else{

            $data = [
                'status' => 'error',
                'code' => '100',
                'message' => 'Usuario no autenticado'
            ];

        }

        return $this->resjson($data);

    }

    public function Listar(Request $request, JwtAuth $jwt_auth){

         /*
           En este metodo se listan todos los registros almacenados en la tabla usuarios.
           ------------------------------------------------------------------------------
           NUTRISYS
        */

        $token = $request->headers->get('Authorization');
        $authCheck = $jwt_auth->checkToken($token);

        if ($authCheck) {
           
            $identity = $jwt_auth->checkToken($token, true);
            $doctrine = $this->getDoctrine();
            $em = $doctrine->getManager();

            //Se obtiene la lista de usuarios
            //================================

            $sqlUsuarios = $em->createQueryBuilder()
               ->select('u') 
               ->from('App\Entity\Usuarios','u')
               ->orderBy('u.id', 'DESC')
            ;
 
           $result = $sqlUsuarios->getQuery()->getResult();

            if(!empty($result)){

                $data = [
                    'status' => 'success',
                    'code' => '200',
                    'message' => 'Lista de usuarios',
                    'data' => $result
                ];

            }else{

               $data = [
                   'status' => 'success',
                   'code' => '300',
                   'message' => 'No se encontraron usuarios para listar',
                   'data' => 0
               ];

            } 

        }else{

                $data = [
                    'status' => 'error',
                    'code' => '100',
                    'message' => 'Usuario no autenticado'
                ];

        }

        return $this->resjson($data);

    }
}
