<?php

namespace App\Controller;

use Knp\Component\Pager\PaginatorInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Validator\Constraints\DateTime;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

use App\Entity\Ingreso;
use App\Services\JwtAuth;

class IngresosController extends AbstractController
{

    private function resjson($data){

        //Metodo para serializar los datos en formato json
        //================================================

        $json = $this->get('serializer')->serialize($data, 'json');
        $response = new Response();
        $response->setContent($json);
        $response->headers->set('Content-Type', 'application/json');

        return $response;

    }
    
    public function Login(Request $request, JwtAuth $jwt_auth)
    {
        /*
            En este método se realiza el ingreso al sistena mediante un usuario y una contraseña
            que son validadas por el sistema.
            ------------------------------------------------------------------------------------
            NUTRISYS
        */

        $doctrine = $this->getDoctrine();
        $em = $doctrine->getManager();

        //Obtener parametros del json
        //===========================

        $json = $request->get('json', null);
        $params = json_decode($json);

        if ($json != null) {

            $usuario = !empty($params->usuario) ? $params->usuario : null;
            $password = !empty($params->password) ? $params->password : null;
            $gettoken = !empty($params->gettoken) ? $params->gettoken : null;

            if (!empty($usuario) && !empty($password)) {

                $pwd = hash('sha256', $password);

                if($gettoken){

                    $signup = $jwt_auth->signup($usuario, $password, $gettoken);

                }else{

                    $signup = $jwt_auth->signup($usuario, $password);

                }

                return new JsonResponse($signup);

            }else{

                $data = [
                    'status' => 'success',
                    'code' => '300',
                    'message' => 'Por favor complete los campos',
                ];

            }

        } else {

            $data = [
                'status' => 'error',
                'code' => '400',
                'message' => 'Json vacío',
            ];

        }

        return $this->resjson($data);
    }
}
