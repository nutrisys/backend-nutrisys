<?php

namespace App\Controller;

use Symfony\Component\Validator\Validation;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Validator\Constraints\DateTime;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

use App\Entity\Indices;
use App\Services\JwtAuth;

class IndicesController extends AbstractController
{

    private function resjson($data){

        //Método para serializar los datos en formato json
        //================================================

        $json = $this->get('serializer')->serialize($data, 'json');
        $response = new Response();
        $response->setContent($json);
        $response->headers->set('Content-Type', 'application/json');

        return $response;

    }
    
    public function Listar(Request $request, JwtAuth $jwt_auth, $opc){

        /*
          En este método se listan todos los registros almacenados en la tabla antropometría.
          -----------------------------------------------------------------------------------
          NUTRISYS
       */

        $token = $request->headers->get('Authorization');
        $authCheck = $jwt_auth->checkToken($token);

        if($authCheck){
            
            $identity = $jwt_auth->checkToken($token, true);
            $doctrine = $this->getDoctrine();
            $em = $doctrine->getManager();

            //Se obtiene parámetro
            //====================

            $idAntropometria = $request->get('idAntropometria');

            switch($opc){

                case 1:

                    //Se obtienen todas los indices registrados
                    //=========================================

                    $sqlIndices = $em->createQueryBuilder()
                        ->select('i') 
                        ->from('App\Entity\Indices','i')
                        ->orderBy('i.id', 'DESC')
                    ;

                    $result = $sqlIndices->getQuery()->getResult();

                    if(!empty($result)){

                        $data = [
                            'status' => 'success',
                            'code' => '200',
                            'message' => 'Lista de indices',
                            'data' => $result
                        ];

                    }else{

                        $data = [
                            'status' => 'success',
                            'code' => '300',
                            'message' => 'No se encontraron indices para listar',
                            'data' => 0
                        ];

                    } 
                    
                break;
                
                case 2:

                    //Se lista un indice por id de antropometrìa
                    //==========================================

                    if(!empty($idAntropometria)){

                        $indice = $doctrine->getRepository(Indices::class)->findOneBy([
                            'antropometriaIdAntropometria' => $idAntropometria
                        ]);

                        if(!empty($indice)){

                            $data = [
                                'status' => 'success',
                                'code' => '200',
                                'message' => 'Información del índice',
                                'data' => $indice
                            ];

                        }else{

                            $data = [
                                'status' => 'success',
                                'code' => '300',
                                'message' => 'No se encontró la antropometría con el id '.$idAntropometria,
                                'data' => 0
                            ];

                        }

                    }else{

                        $data = [
                            'status' => 'success',
                            'code' => '300',
                            'message' => 'Por favor ingrese un id de antropometía',
                        ];

                    } 
                   
                break;

            }

        }else{
            
            $data = [
                'status' => 'error',
                'code' => '100',
                'message' => 'Usuario no autenticado'
            ];

        }

        return $this->resjson($data);

    }

}
