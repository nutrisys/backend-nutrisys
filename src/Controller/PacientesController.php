<?php

namespace App\Controller;

use Symfony\Component\Validator\Validation;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Validator\Constraints\DateTime;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

use App\Entity\Control;
use App\Entity\Paciente;
use App\Entity\Usuarios;
use App\Services\JwtAuth;

class PacientesController extends AbstractController
{

    private function resjson($data){

        //Método para serializar los datos en formato json
        //================================================

        $json = $this->get('serializer')->serialize($data, 'json');
        $response = new Response();
        $response->setContent($json);
        $response->headers->set('Content-Type', 'application/json');

        return $response;

    }

    public function Registrar(Request $request, JwtAuth $jwt_auth){

        /*
            En este método se realiza el registro de pacientes y se guarda en el campo usuario
            la información del médico que realice esta operacion.
            -----------------------------------------------------------------------------------
            NUTRISYS
        */

        $token = $request->headers->get('Authorization');
        $authCheck = $jwt_auth->checkToken($token);

        if($authCheck){
            
            $identity = $jwt_auth->checkToken($token, true);
            $doctrine = $this->getDoctrine();
            $em = $doctrine->getManager();

            //Obtener parametros del json
            //===========================

            $json = $request->get('json', null);
            $params = json_decode($json);
            
            $medico = $doctrine->getRepository(Usuarios::class)->findOneBy([
                'id' => $identity->sub
            ]);

            $fecha = new \DateTime('now', new \DateTimeZone('America/Bogota'));
            $fechaControl = $fecha->format('Y-m-d H:i:s');

            if($json != null){

                // $edad = !empty($params->edad) ? $params->edad : null;
                $email = !empty($params->email) ? $params->email : null;
                $genero = !empty($params->genero) ? $params->genero : null;
                $nombres = !empty($params->nombres) ? $params->nombres : null;
                $telefono = !empty($params->telefono) ? $params->telefono : null;
                $apellidos = !empty($params->apellidos) ? $params->apellidos : null;
                $direccion = !empty($params->direccion) ? $params->direccion : null;
                $documento = !empty($params->documento) ? $params->documento : null;
                $fechaNacimiento = !empty($params->fechaNacimiento) ? $params->fechaNacimiento : null;

                if (!empty($nombres) && !empty($apellidos) && !empty($genero) && !empty($documento) && !empty($fechaNacimiento) && !empty($email)
                    && !empty($direccion) && !empty($telefono)){

                    $nombresTrim = trim($nombres);
                    $nombres = $nombresTrim;

                    $apellidosTrim = trim($apellidos);
                    $apellidos = $apellidosTrim;

                    $documentUnique = $doctrine->getRepository(Paciente::class)->findOneBy([
                        'identificacion' => $documento
                    ]);

                    if(empty($documentUnique)){

                        //Registro de paciente
                        //====================

                        $paciente = new Paciente();
                        // $paciente->setEdad($edad);
                        $paciente->setEmail($email);
                        $paciente->setGenero($genero);
                        $paciente->setUsuario($medico);
                        $paciente->setTelefono($telefono);
                        $paciente->setDireccion($direccion);
                        $paciente->setIdentificacion($documento);
                        $paciente->setNombres(strtoupper($nombres));
                        $paciente->setApellidos(strtoupper($apellidos));
                        $paciente->setFechaNacimiento($fechaNacimiento);

                        $em->persist($paciente);
                        $em->flush();

                        //Se registra el paciente en Control
                        //==================================

                        $control = new Control();
                        $control->setUsuario($medico);
                        $control->setFecha($fechaControl);
                        $control->setPacienteIdPaciente($paciente);

                        $em->persist($control);
                        $em->flush();

                        $dataPaciente = [
                            'idControl' => $control->getId(),
                            'paciente' => $paciente
                        ];
        
                        $data = [
                            'status' => 'success',
                            'code' => '200',
                            'message' => 'Paciente registrado con éxito',
                            'data' => $dataPaciente
                        ];

                    }else{

                        $data = [
                            'status' => 'success',
                            'code' => '300',
                            'message' => 'Ya existe un paciente con este número de documento'
                        ];
                    
                    }

                }else{

                    $data = [
                        'status' => 'success',
                        'code' => '300',
                        'message' => 'Por favor complete todos los campos',
                    ];

                }

            }else{

                $data = [
                    'status' => 'error',
                    'code' => '400',
                    'message' => 'Json vacío',
                ];

            }

        }else{

            $data = [
                'status' => 'error',
                'code' => '100',
                'message' => 'Usuario no autenticado'
            ];

        }

        return $this->resjson($data);

    }

    public function Actualizar(Request $request, JwtAuth $jwt_auth){

        /*
            En este método se actualiza la información de un paciente y se guarda en el campo usuario
            la información del médico que realice esta operacion.
            ------------------------------------------------------------------------------------------
            NUTRISYS
        */

        $token = $request->headers->get('Authorization');
        $authCheck = $jwt_auth->checkToken($token);

        if($authCheck){
            
            $identity = $jwt_auth->checkToken($token, true);
            $doctrine = $this->getDoctrine();
            $em = $doctrine->getManager();

            //Obtenemos parámetro
            //===================

            $idPaciente = $request->get('idPaciente');

            if(!empty($idPaciente)){

                $paciente = $doctrine->getRepository(Paciente::class)->findOneBy([
                    'id' => $idPaciente
                ]);

                if(!empty($paciente)){

                    //Obtener parametros del json
                    //===========================

                    $json = $request->get('json', null);
                    $params = json_decode($json);
                    
                    $medico = $doctrine->getRepository(Usuarios::class)->findOneBy([
                        'id' => $identity->sub
                    ]);

                    if($json != null){

                        // $edad = !empty($params->edad) ? $params->edad : $paciente->getEdad();
                        $email = !empty($params->email) ? $params->email : $paciente->getEmail();
                        $genero = !empty($params->genero) ? $params->genero : $paciente->getGenero();
                        $nombres = !empty($params->nombres) ? $params->nombres : $paciente->getNombres();
                        $telefono = !empty($params->telefono) ? $params->telefono : $paciente->getTelefono();
                        $apellidos = !empty($params->apellidos) ? $params->apellidos : $paciente->getApellidos();
                        $direccion = !empty($params->direccion) ? $params->direccion : $paciente->getDireccion();
                        $documento = !empty($params->documento) ? $params->documento : $paciente->getIdentificacion();
                        $fechaNacimiento = !empty($params->fechaNacimiento) ? $params->fechaNacimiento : $paciente->getFechaNacimiento();
     
                        $nombresTrim = trim($nombres);
                        $nombres = $nombresTrim;

                        $apellidosTrim = trim($apellidos);
                        $apellidos = $apellidosTrim;

                        //Se verifica la existencia del documento
                        //=======================================

                        if($documento == $paciente->getIdentificacion()){

                            $estadoPaciente = 1;

                        }else{

                            $documentUnique = $doctrine->getRepository(Paciente::class)->findOneBy([
                                'identificacion' => $documento
                            ]);

                            if(!empty($documentUnique)){

                                $estadoPaciente = 0;

                            }else{

                                $estadoPaciente = 1;

                            }

                        }

                        if($estadoPaciente > 0){

                            //Actualización de paciente
                            //=========================

                            // $paciente->setEdad($edad);
                            $paciente->setEmail($email);
                            $paciente->setGenero($genero);
                            $paciente->setUsuario($medico);
                            $paciente->setTelefono($telefono);
                            $paciente->setDireccion($direccion);
                            $paciente->setIdentificacion($documento);
                            $paciente->setNombres(strtoupper($nombres));
                            $paciente->setApellidos(strtoupper($apellidos));
                            $paciente->setFechaNacimiento($fechaNacimiento);

                            $em->persist($paciente);
                            $em->flush();
            
                            $data = [
                                'status' => 'success',
                                'code' => '200',
                                'message' => 'Paciente actualizado con éxito',
                                'data' => $paciente
                            ];

                        }else{

                            $data = [
                                'status' => 'success',
                                'code' => '300',
                                'message' => 'Ya existe un paciente con este número de documento'
                            ];
                        
                        }
                       
                    }else{

                        $data = [
                            'status' => 'error',
                            'code' => '400',
                            'message' => 'Json vacío',
                        ];

                    }

                }else{

                    $data = [
                        'status' => 'success',
                        'code' => '300',
                        'message' => 'No se encontró el paciente con el id '.$idPaciente,
                        'data' => 0
                    ];

                }

            }else{

                $data = [
                    'status' => 'success',
                    'code' => '300',
                    'message' => 'Por favor ingrese un id de paciente'
                ];

            }

        }else{

            $data = [
                'status' => 'error',
                'code' => '100',
                'message' => 'Usuario no autenticado'
            ];

        }

        return $this->resjson($data);

    }

    public function Listar(Request $request, JwtAuth $jwt_auth, $opc){

        /*
          En este metodo se listan todos los registros almacenados en la tabla pacientes.
          -------------------------------------------------------------------------------
          NUTRISYS
        */

        $token = $request->headers->get('Authorization');
        $authCheck = $jwt_auth->checkToken($token);

        if($authCheck){
          
            $identity = $jwt_auth->checkToken($token, true);
            $doctrine = $this->getDoctrine();
            $em = $doctrine->getManager();

            //Se obtiene parámetro
            //====================

            $documento = $request->get('documento');

            switch($opc){
               
                case 1:

                    //Se obtiene la lista de todos los pacientes registrados
                    //======================================================

                    if($identity->rol == 'medico'){

                        $sqlPacientes = $em->createQueryBuilder()
                            ->select('p') 
                            ->from('App\Entity\Paciente','p')
                            ->where('p.usuario = :usuario')
                            ->setParameter('usuario', $identity->sub)
                            ->orderBy('p.id', 'DESC')
                        ;

                    }else{

                        $sqlPacientes = $em->createQueryBuilder()
                            ->select('p') 
                            ->from('App\Entity\Paciente','p')
                            ->orderBy('p.id', 'DESC')
                        ;

                    }

                    $result = $sqlPacientes->getQuery()->getResult();
                    
                    if(!empty($result)){

                        $data = [
                            'status' => 'success',
                            'code' => '200',
                            'message' => 'Lista de pacientes',
                            'data' => $result
                        ];

                    }else{

                        $data = [
                            'status' => 'success',
                            'code' => '300',
                            'message' => 'No se encontraron pacientes para listar',
                            'data' => 0
                        ];

                    } 
                  
                break;
               
                case 2:

                    //Se lista un cliente por número de documento
                    //===========================================

                    if(!empty($documento)){

                        $paciente = $doctrine->getRepository(Paciente::class)->findOneBy([
                            'identificacion' => $documento
                        ]);

                        if(!empty($paciente)){

                            $controles = $doctrine->getRepository(Control::class)->findBy([
                                'pacienteIdPaciente' => $paciente->getId()
                            ]);

                            if(!empty($controles)){

                                $dataPaciente = [
                                    'paciente' => $paciente,
                                    'controles' => $controles
                                ];

                                $data = [
                                    'status' => 'success',
                                    'code' => '200',
                                    'message' => 'Información del paciente',
                                    'data' => $dataPaciente
                                ];

                            }else{

                                $data = [
                                    'status' => 'success',
                                    'code' => '300',
                                    'message' => 'No se encontraron controles para este paciente',
                                    'data' => 0
                                ];

                            }        

                        }else{

                            $data = [
                                'status' => 'success',
                                'code' => '300',
                                'message' => 'No se encontró el paciente con el número de documento '.$documento,
                                'data' => 0
                            ];

                        }

                    }else{

                        $data = [
                            'status' => 'success',
                            'code' => '300',
                            'message' => 'Por favor ingrese un número de documento'
                        ];

                    }
                    
                break;

            }  

        }else{

                $data = [
                   'status' => 'error',
                   'code' => '100',
                   'message' => 'Usuario no autenticado'
                ];

        }

       return $this->resjson($data);

    }

    public function ControlesPorPaciente(Request $request, JwtAuth $jwt_auth){

        /*
          En este método se listan todos los controles que le pertenecen a un cliente.
          ----------------------------------------------------------------------------
          NUTRISYS
        */

        $token = $request->headers->get('Authorization');
        $authCheck = $jwt_auth->checkToken($token);

        if($authCheck){
          
            $identity = $jwt_auth->checkToken($token, true);
            $doctrine = $this->getDoctrine();
            $em = $doctrine->getManager();

            //Se obtiene parámetro
            //====================

            $idPaciente = $request->get('idPaciente');

            if(!empty($idPaciente)){

                $paciente = $doctrine->getRepository(Paciente::class)->findOneBy([
                    'id' => $idPaciente
                ]);

                if(!empty($paciente)){

                    $sqlControles = $em->createQueryBuilder()
                        ->select('c') 
                        ->from('App\Entity\Control','c')
                        ->where('c.pacienteIdPaciente = :paciente')
                        ->setParameter('paciente', $idPaciente)
                        ->orderBy('c.id', 'DESC')
                    ;

                    $result = $sqlControles->getQuery()->getResult();

                    if(!empty($result)){

                        $data = [
                            'status' => 'success',
                            'code' => '200',
                            'message' => 'Lista de controles del paciente',
                            'data' => $result
                        ];

                    }else{

                        $data = [
                            'status' => 'success',
                            'code' => '300',
                            'message' => 'No se encontraron controles para este paciente',
                            'data' => 0
                        ];

                    }             

                }else{

                    $data = [
                        'status' => 'success',
                        'code' => '300',
                        'message' => 'No se encontró el paciente con el id '.$idPaciente,
                        'data' => 0
                    ];

                }

            }else{

                $data = [
                    'status' => 'success',
                    'code' => '300',
                    'message' => 'Por favor ingrese un id de paciente'
                ];

            }

        }else{

                $data = [
                   'status' => 'error',
                   'code' => '100',
                   'message' => 'Usuario no autenticado'
                ];

        }

       return $this->resjson($data);

    }

}
