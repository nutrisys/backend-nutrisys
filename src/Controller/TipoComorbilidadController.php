<?php

namespace App\Controller;

use Symfony\Component\Validator\Validation;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Validator\Constraints\DateTime;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

use App\Entity\Control;
use App\Services\JwtAuth;
use App\Entity\Perfilbioquimco;
use App\Entity\Tipocomorbilidad;

class TipoComorbilidadController extends AbstractController
{
    
    private function resjson($data){

        //Método para serializar los datos en formato json
        //================================================

        $json = $this->get('serializer')->serialize($data, 'json');
        $response = new Response();
        $response->setContent($json);
        $response->headers->set('Content-Type', 'application/json');

        return $response;

    }

    public function Registrar(Request $request, JwtAuth $jwt_auth){

        /*
            En este método se realiza el registro de comorbilidades.
            --------------------------------------------------------
            NUTRISYS
        */

        $token = $request->headers->get('Authorization');
        $authCheck = $jwt_auth->checkToken($token);

        if($authCheck){
            
            $identity = $jwt_auth->checkToken($token, true);
            $doctrine = $this->getDoctrine();
            $em = $doctrine->getManager();

            //Obtener parametros del json
            //===========================

            $json = $request->get('json', null);
            $params = json_decode($json);

            if($json != null){

                $idControl = !empty($params->idControl) ? $params->idControl : null;
                $descripcion = !empty($params->descripcion) ? $params->descripcion : '';
                $perfilBioquimico = !empty($params->perfilBioquimico) ? $params->perfilBioquimico : null;

                if(!empty($perfilBioquimico) && !empty($descripcion) && !empty($idControl)){

                    $control = $doctrine->getRepository(Control::class)->findOneBy([
                        'id' => $idControl
                    ]);

                    $perfilBioquimico_ = $doctrine->getRepository(Perfilbioquimco::class)->findOneBy([
                        'id' => $perfilBioquimico
                    ]);

                    if(!empty($perfilBioquimico_)){

                        //Registro de comorbilidad
                        //========================

                        $comorbilidad = new Tipocomorbilidad();
                        $comorbilidad->setDescripcion($descripcion);
                        $comorbilidad->setPerfilbioquimcoIdperfilbioquimco($perfilBioquimico_);

                        $em->persist($comorbilidad);
                        $em->flush();

                        //Se actulizan la fkey de comorbilidad
                        //==================================== 

                        $control->setIdTipocomorbilidad($comorbilidad);
                        $em->persist($control);
                        $em->flush();
        
                        $data = [
                            'status' => 'success',
                            'code' => '200',
                            'message' => 'Comorbilidad registrada con éxito',
                            'data' => $comorbilidad
                        ];

                    }else{

                        $data = [
                            'status' => 'success',
                            'code' => '300',
                            'message' => 'No se encontró el perfil bioquímico con el id '.$perfilBioquimico,
                            'data' => 0
                        ];
                    
                    }

                }else{

                    $data = [
                        'status' => 'success',
                        'code' => '300',
                        'message' => 'Por favor complete todos los campos',
                    ];

                }

            }else{

                $data = [
                    'status' => 'error',
                    'code' => '400',
                    'message' => 'Json vacío',
                ];

            }

        }else{

            $data = [
                'status' => 'error',
                'code' => '100',
                'message' => 'Usuario no autenticado'
            ];

        }

        return $this->resjson($data);

    }

}
