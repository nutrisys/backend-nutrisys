<?php

namespace App\Controller;

use Symfony\Component\Validator\Validation;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Validator\Constraints\DateTime;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

use App\Entity\Control;
use App\Services\JwtAuth;
use App\Entity\Intervencion;
use App\Entity\TipoIntervencion;

class IntervencionesController extends AbstractController
{
    private function resjson($data){

        //Método para serializar los datos en formato json
        //================================================

        $json = $this->get('serializer')->serialize($data, 'json');
        $response = new Response();
        $response->setContent($json);
        $response->headers->set('Content-Type', 'application/json');

        return $response;

    }

    public function Registrar(Request $request, JwtAuth $jwt_auth){

        /*
            En este método se realiza el registro de intervenciones y tipo intervención.
            ----------------------------------------------------------------------------
            NUTRISYS
        */

        $token = $request->headers->get('Authorization');
        $authCheck = $jwt_auth->checkToken($token);

        if($authCheck){
            
            $identity = $jwt_auth->checkToken($token, true);
            $doctrine = $this->getDoctrine();
            $em = $doctrine->getManager();

            //Obtener parametros del json
            //===========================

            $json = $request->get('json', null);
            $params = json_decode($json);

            if($json != null){

                $idControl = !empty($params->idControl) ? $params->idControl : null;
                $observacion = !empty($params->observacion) ? $params->observacion : '';
                $descripcion = !empty($params->descripcion) ? $params->descripcion : '';

                if(!empty($idControl)){

                    $control = $doctrine->getRepository(Control::class)->find($idControl);

                    if(!empty($control)){

                        //Registro de intervención
                        //========================

                        $intervencion = new Intervencion();
                        $intervencion->setDescripcion($descripcion);
                        $em->persist($intervencion);
                        $em->flush();

                        //Una vez se registra la intervención sea crea un tipo intervención
                        //=================================================================

                        $tipoIntervencion = new TipoIntervencion();
                        $tipoIntervencion->setObservacion($observacion);
                        $tipoIntervencion->setIntervencion($intervencion);
                        $em->persist($tipoIntervencion);
                        $em->flush();

                        //Se actulizan las fkey de tipoIntervencion
                        //========================================= 

                        $control->setTipoIntervencion($tipoIntervencion);
                        $em->persist($control);
                        $em->flush();

                        $data = [
                            'status' => 'success',
                            'code' => '200',
                            'message' => 'Intervención registrada con éxito',
                            'data' => $intervencion
                        ];

                    }else{

                        $data = [
                            'status' => 'success',
                            'code' => '300',
                            'message' => 'No se encontró el control con el id '.$idControl,
                            'data' => 0
                        ];

                    }

                }else{

                    $data = [
                        'status' => 'success',
                        'code' => '300',
                        'message' => 'Por favor ingrese un id de control'
                    ];

                }

            }else{

                $data = [
                    'status' => 'error',
                    'code' => '400',
                    'message' => 'Json vacío',
                ];

            }

        }else{

            $data = [
                'status' => 'error',
                'code' => '100',
                'message' => 'Usuario no autenticado'
            ];

        }

        return $this->resjson($data);

    }

}
