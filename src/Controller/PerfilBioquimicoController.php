<?php

namespace App\Controller;

use Symfony\Component\Validator\Validation;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Validator\Constraints\DateTime;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

use App\Entity\Control;
use App\Services\JwtAuth;
use App\Entity\Perfilbioquimco;

class PerfilBioquimicoController extends AbstractController
{
    
    private function resjson($data){

        //Método para serializar los datos en formato json
        //================================================

        $json = $this->get('serializer')->serialize($data, 'json');
        $response = new Response();
        $response->setContent($json);
        $response->headers->set('Content-Type', 'application/json');

        return $response;

    }
    
    public function Registrar(Request $request, JwtAuth $jwt_auth){

        /*
            En este método se realiza el registro de perfilbioquimico.
            ----------------------------------------------------------
            NUTRISYS
        */

        $token = $request->headers->get('Authorization');
        $authCheck = $jwt_auth->checkToken($token);

        if($authCheck){
            
            $identity = $jwt_auth->checkToken($token, true);
            $doctrine = $this->getDoctrine();
            $em = $doctrine->getManager();

            //Obtener parametros del json
            //===========================

            $json = $request->get('json', null);
            $params = json_decode($json); 

            if($json != null){

                $homair = !empty($params->homair) ? $params->homair : null;
                $leptina = !empty($params->leptina) ? $params->leptina : null;
                $insulina = !empty($params->insulina) ? $params->insulina : null;
                $glucemia = !empty($params->glucemia) ? $params->glucemia : null;
                $idControl = !empty($params->idControl) ? $params->idControl : null;

                if(!empty($glucemia) && !empty($insulina) && !empty($homair) && !empty($leptina) && !empty($idControl)){
                   
                    $control = $doctrine->getRepository(Control::class)->findOneBy([
                        'id' => $idControl
                    ]);

                    //Registro del perfil bioquimico
                    //==============================

                    $perfil = new Perfilbioquimco();
                    $perfil->setHomair($homair);
                    $perfil->setLeptina($leptina);
                    $perfil->setInsulina($insulina);
                    $perfil->setGlucemia($glucemia);

                    $em->persist($perfil);
                    $em->flush();

                    //Se actulizan la fkey de comorbilidad
                    //==================================== 

                    $control->setPerfilbioquimcoIdperfilbioquimco($perfil);
                    $em->persist($control);
                    $em->flush();
    
                    $data = [
                        'status' => 'success',
                        'code' => '200',
                        'message' => 'Perfil bioquímico registrado con éxito',
                        'data' => $perfil
                    ];
         
                }else{

                    $data = [
                        'status' => 'success',
                        'code' => '300',
                        'message' => 'Por favor complete todos los campos',
                    ];

                }

            }else{

                $data = [
                    'status' => 'error',
                    'code' => '400',
                    'message' => 'Json vacío',
                ];

            }

        }else{

            $data = [
                'status' => 'error',
                'code' => '100',
                'message' => 'Usuario no autenticado'
            ];

        }

        return $this->resjson($data);

    }

}
