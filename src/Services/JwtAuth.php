<?php

namespace App\Services;
use Doctrine\ORM\EntityManagerInterface;

use Firebase\JWT\JWT;
use App\Entity\Ingreso;

class JwtAuth
{

    public $entityManager;
    private $repositoryIngreso;

    public $manager;
    public $key = 'Nutrisys_27122019';

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
        $this->repositoryIngreso = $entityManager->getRepository(Ingreso::class);
    }

    public function signup($usuario, $password, $gettoken = null){

        /*
            En este método se valida el acceso a sistema mediante un usuario y una contraseña.
            ----------------------------------------------------------------------------------
            NUTRISYS
        */

        //Se verifica la existencia del usuario
        //=====================================

        $pwd = hash('sha256', $password);

        $usuarioIngreso = $this->repositoryIngreso->findOneBy([
            'usuario' => $usuario,
            'contrasena' => $pwd
        ]);

        if(!empty($usuarioIngreso)){

            $token = [
                'sub' => $usuarioIngreso->getUsuarioId()->getId(),
                'nombres' => $usuarioIngreso->getUsuarioId()->getNombre(),
                'apellidos' => $usuarioIngreso->getUsuarioId()->getApellido(),
                'documento' => $usuarioIngreso->getUsuarioId()->getIdentificacion(),
                'rol' => $usuarioIngreso->getUsuarioId()->getRol(),
                'iat' => time(),
                'exp' => time() + (7 * 24 * 60 * 60)
            ];

            //Token encriptado
            //================

            $jwt = JWT::encode($token, $this->key, 'HS256');

            if(!empty($gettoken)){

                $data = $jwt;

            }else{

                //Token desencriptado
                //===================

                $decoded = JWT::decode($jwt, $this->key, ['HS256']);
                $data = $decoded;

                $data = [
                    'status' => 'success',
                    'message' => 'Se ha iniciado sesión correctamente',
                    'data' => $decoded
                ];

            }

        }else{

            $data = [
                'status' => 'error',
                'message' => 'El usuario o la contraseña ingresados son incorrectos'
            ];

        }

        return $data;

    }

    public function checkToken($jwt, $identity = false)
    {
        $auth = false;
        try {
            $decoded = JWT::decode($jwt, $this->key, ['HS256']);
        } catch (\UnexpectedValueException $e) {
            $auth = false;
        } catch (\DomainException $e) {
            $auth = false;
        }
        if (isset($decoded) && !empty($decoded) && is_object($decoded) && isset($decoded->sub)) {
            $auth = true;
        } else {
            $auth = false;
        }
        if ($identity != false) {
            return $decoded;
        } else {
            return $auth;
        }
    }
}
